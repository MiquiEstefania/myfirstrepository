#include<string.h>
#include<iostream>
#include<windows.h>
#include<stdlib.h>
#include<time.h>
#define FILA 6 //NUMERO DE PALABRAS
#define TAM 10 //TAMA�O DE CARACTERES DE CADA PALABRA
using namespace std;

void head();
void menu1();
void menu2();
void ingresar(char cadena[][TAM], int tam_vec);
void mostrar(char cadena[][TAM],int tam_vec);
int aleatorio(int tam_vec), longitud=0;
void rotulo(int a);
void gotoxy(short x, short y);
void base();
void cabeza();
void cuerpo();
void pie_izquierdo();
void pie_derecho();
void mano_derecha();
void mano_izquierda();
void ganar1();
void autores ();

int main(){

    srand(time(NULL));
    int op=0,op2=0,al=0,intentos=0,cont=0,e;
    char nombres[FILA][TAM],elegido[TAM],letra,i=173,respuesta[TAM];

    do{
        head();
        menu1();
        autores();

        do{
            gotoxy(33,11); cin >> op;
        }while(op==0 || op>=4);

        system("cls");
        switch(op){

            case 1:
                do{
                        menu2();
                        do{
                            gotoxy(35,7); cin >> op2;
                        }while(op2==0 || op2>=4);
                        system("cls");

                        switch(op2){

                            case 1:
                                ingresar(nombres,FILA);
                                break;
                            case 2:
                                mostrar(nombres,FILA);
                                system("PAUSE");
                                break;
                        }
                        system("cls");
                    }while(op2!=3);
                break;

            case 2:
                al = aleatorio(FILA);
                strcpy(elegido,nombres[al]);
                longitud = strlen(elegido);

                gotoxy(30,1); cout << "\t ** EL AHORCADO **";
                base();
                rotulo(0);
                e=0;
                cont=0;
                gotoxy(7,3); cout << i << " A JUGAR !:";
                gotoxy(7,5); cout << "LA PALABRA ESCOGIDA TIENE " << longitud << " CARACTERES";
                gotoxy(7,37);
                for (int i=0; i<longitud ; i++){
                    respuesta[i]='*';
                    cout << respuesta[i];
                }
                do{
                int error=0;
                gotoxy(7,39); cout << "INGRESE UNA LETRA: ";
                              cin >> letra;
                gotoxy(7,37);
                    for(int j=0; j<longitud; j++){
                        if (letra==elegido[j]){
                            if (letra!=respuesta[j]){
                            respuesta[j]=letra;
                            cont++;
                            }
                        } else {
                            error++;
                        }
                    cout << respuesta[j];
                    }
                if(error==longitud){
                intentos++;
                }
                rotulo(intentos);
                switch (intentos){
                    case 1:
                        cabeza();
                        break;
                    case 2:
                        cuerpo();
                        break;
                    case 3:
                        mano_derecha();
                        break;
                    case 4:
                        mano_izquierda();
                        break;
                    case 5:
                        pie_izquierdo();
                        break;
                    case 6:
                        pie_derecho();
                        break;
                }
                }while(cont<longitud && intentos<6);
                if(intentos==6){
                    gotoxy(10,41); cout << "*** LO SIENTO, HA PERDIDO ***";
                    gotoxy(9,42); cout << " PARA UNA NUEVA PARTIDA, CARGUE NUEVAS PALABRAS" << endl;
                    gotoxy(7,36);
                    for(int j=0; j<longitud; j++){
                        cout << elegido[j];
                    }
                    cout << endl;
                }
                if(cont==longitud){
                    if(intentos==0){
                        ganar1();
                    }else{
                    gotoxy(10,41); cout << "*** FELICIDADES! GANO CON " << intentos << " INTENTOS FALLIDOS ***" << endl;
                    gotoxy(9,42); cout << " PARA UNA NUEVA PARTIDA, CARGUE NUEVAS PALABRAS" << endl;
                    }
                }
                gotoxy(9,44); system("PAUSE");
                break;
        }
    system("cls");
    }while(op!=3);

return 0;
}

void head(){
    system("color 2f");
    gotoxy(30,1);
    Sleep(900);
    system("color 2f"); cout << "* ";
    Sleep(900);
    system("color f2"); cout << "* ";
    Sleep(900);
    system("color 2f"); cout << " EL  AHORCADO  ";
    Sleep(900);
    system("color f2"); cout << " *";
    Sleep(900);
    system("color 2f"); cout << " *";
    Sleep(900);
}
void menu1(){
    gotoxy(5,4); Sleep(700); cout << "1. CARGAR PALABRAS";
    gotoxy(5,5); Sleep(700); cout << "2. JUGAR";
    gotoxy(5,6); Sleep(700); cout << "3. SALIR";
}
void menu2(){
    gotoxy(30,1); cout << "*** CARGAR PALABRAS  ***";
    gotoxy(5,3); cout << "1. INGRESAR PALABRAS";
    gotoxy(5,4); cout << "2. VISUALIZAR";
    gotoxy(5,5); cout << "3. REGRESAR AL MENU PRINCIPAL";
    gotoxy(5,7); cout << "INTRODUZCA UNA OPCION (1-3): ";
}
void ingresar(char cadena[][TAM], int tam_vec){
        gotoxy(30,1); cout << "** CARGA DE PALABRAS **";
        for(int i=0; i<tam_vec; i++){
            cout << endl << endl << "\tINGRESE UNA PALABRA:" << endl << "\t";
            cin >> cadena[i];
        }
}
void mostrar(char cadena[][TAM],int tam_vec){
    gotoxy(30,1); cout << "** VISUALIZAR PALABRAS **";
    for(int i=0; i<tam_vec; i++){
            cout << endl << "\t" << cadena[i] << endl;
    }
    cout << endl;
}
int aleatorio(int tam_vec){
    int num;
    num=rand()%tam_vec;
    return num;
}
void rotulo(int a){
    int vidas=0;
    vidas=6-a;
        gotoxy(47,3); cout << " _____________________________";
        gotoxy(47,4); cout << "|                             |";
        gotoxy(47,5); cout << "|   INTENTOS RESTANTES: " << vidas << "     |";
        gotoxy(47,6); cout << "|_____________________________|";
}
void gotoxy(short x, short y) {
 HANDLE hConsoleOutput;
 COORD Cursor_Pos = {x, y};
 hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
 SetConsoleCursorPosition(hConsoleOutput, Cursor_Pos);
}
void base(){
    int x,y;
    //system("mode 80");
    for(x=8;x<=33;x++){
        gotoxy(16,x); cout << "*";
    }
    for(y=17;y<=26;y++){
        gotoxy(y,8); cout << "*";
    }
    for(y=11;y<=21;y++){
        gotoxy(y,34); cout << "*";
    }
 gotoxy(26,9); cout << "*";
 gotoxy(26,10); cout << "*";
 }
void cabeza(){
    for(int x=0;x<5;x++){
            gotoxy(24+x,11); cout << "*";
            gotoxy(24+x,15); cout << "*";
    }
    gotoxy(25,12);
    for(int y=0;y<3;y++){
            gotoxy(24,12+y); cout << "*";
            gotoxy(28,12+y); cout << "*";
    }
}
void cuerpo(){
    for(int y=0;y<9;y++){
            gotoxy(26,16+y); cout << "*";
    }
}
void pie_izquierdo(){
    for(int x=0;x<3;x++){
            gotoxy(27+x,24+x); cout << "*";
    }
}
void pie_derecho(){
    for(int x=0;x<3;x++){
            gotoxy(25-x,24+x); cout << "*";
    }
}
void mano_derecha(){
    for(int y=0;y<3;y++){
            gotoxy(25-y,16+y); cout << "*";
    }
}
void mano_izquierda(){
    for(int x=0;x<3;x++){
            gotoxy(29-x,18-x); cout << "*";
    }
}
void ganar1(){
    gotoxy(10,41); Sleep(700); cout << "*** FELICIDADES! GANO SIN COMETER ERRORES ***" << endl;
    gotoxy(9,42); Sleep(700);  cout << " PARA UNA NUEVA PARTIDA, CARGUE NUEVAS PALABRAS" << endl;
}
void autores (){
    system("color 2f");
    Sleep(500);
	gotoxy(45,3); cout << " _____________________________";
    gotoxy(45,4); cout << "|                             |";
    gotoxy(45,5); cout << "|       REALIZADO POR:        |";
    gotoxy(45,6); cout << "|       DAYANNA SILVA         |";
    gotoxy(45,7); cout << "|             &               |";
    gotoxy(45,8); cout << "|     MIQUELY CALVOPINA       |";
    gotoxy(45,9); cout << "|_____________________________|";

    Sleep(700); gotoxy(5,11); cout << "INGRESE UNA OPCION (1-3): ";
}
